
// call RESTful API just created, get the data back and loop through it
// for each state in it, create an option elment with value of abbreviation
// and text of the name. Use app.JS for refrence

//Add an event listener for when the DOM loads
window.addEventListener('DOMContentLoaded', async () => {
    // declare a variable that will hold the URL for the API that we just created
    const url = 'http://localhost:8000/api/states/';
    // fetch the URL. Use await keyword so that we get the response, not the Promise
    try {
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();

            const selectTag = document.getElementById("state");
            for (let state of data.states) {
                const option = document.createElement("option");
                option.value = state.abbreviations;
                option.innerHTML = state.name;
                selectTag.appendChild(option);
            }
        }
    } catch (e) {
        console.error(e)
    }
        const formTag = document.getElementById("create-location-form");
        formTag.addEventListener("submit", async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const locationUrl = 'http://localhost:8000/api/locations/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();
            }
        })
})
