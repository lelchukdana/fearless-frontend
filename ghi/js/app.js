function createCard(name, description, pictureUrl, starts, ends, location) {

    return `
    <div class="card m-4">
      <div class="card shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          </div class="card-footer">
          ${new Date(starts).toLocaleDateString()}-${new Date(ends).toLocaleDateString()}
          </div>
        </div>
      </div>
    `;
  }
  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    console.log("anything")
    try {
        console.log("any")
      const response = await fetch(url);
      if (!response.ok) {
        const errormessage = `
        <div class="alert alert-warning" role="alert">
        There was an issue when trying to fetch the conference URL!
        `

      } else {
        const data = await response.json();

        let index = 0;
        for (let conference of data.conferences) {
          // const conference = data.conferences[0];
          // const nameTag = document.querySelector('.card-title')
          // nameTag.innerHTML = conference.name
          const detailUrl = `http://localhost:8000${conference.href}`
          const detailResponse = await fetch(detailUrl)
          if (detailResponse.ok) {
              const details = await detailResponse.json()
              console.log(details)
              const title = details.conference.name
              const description = details.conference.description
              // description.innerHTML = details.conference.description
              const pictureUrl = details.conference.location.picture_url
              // image.src = details.conference.location.picture_url
              const starts = details.conference.starts
              const ends = details.conference.ends
              const location = details.conference.location.name
              const html = createCard(title, description, pictureUrl,starts,ends,location)
              // Created the Card HTML
              const column = document.querySelector(`#col-${index % 3}`);
              column.innerHTML += html;
              index += 1;
              // if (index > 2) {
              //   index = 0
              // }
          }
        }
      }
    } catch (e) {
        console.error(e)
    }
  });
